﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TableTennisManager.Bal.Services;
using TableTennisManager.Contracts;

namespace TableTennisManager.Bal.Tests.Services
{
    [TestClass]
    public class RatingServiceTests
    {
        private RatingService _ratingService;
        private IPlayerRepository _playerRepository;
        private IMatchRepository _matchRepository;

        [TestInitialize]
        public void Init()
        {
            _playerRepository = new FakePlayerRepository();
            _matchRepository = new FakeMatchRepository();
            _ratingService = new RatingService(_playerRepository, _matchRepository);
        }

        [TestMethod]
        public void GeneratePlayerRatingForPlayerTest()
        {
            var players = _ratingService.GenerateAllPlayerRatings();

            foreach (var player in players)
            {
                Console.WriteLine(player.PlayerRating);
            }
        }
    }
}