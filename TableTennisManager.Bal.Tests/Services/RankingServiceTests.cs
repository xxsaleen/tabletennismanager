﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TableTennisManager.Bal.Services;
using TableTennisManager.Contracts;
using TableTennisManager.Models;

namespace TableTennisManager.Bal.Tests.Services
{
    [TestClass]
    public class RankingServiceTests
    {
        private RankingService _rankingService;
        private IPlayerRepository _playerRepository;
        private IMatchRepository _matchRepository;

        [TestInitialize]
        public void Init()
        {
            _playerRepository = new FakePlayerRepository();
            _matchRepository = new FakeMatchRepository();
            _rankingService = new RankingService(_matchRepository, _playerRepository);
        }

        [TestMethod]
        public void ComputePlayerRankingsTest()
        {
            _rankingService.CalculatePlayerRankings();
        }

        [TestMethod]
        public void CalculatePlayerRankingPointsTest()
        {
            var rankings = _rankingService.CalculateAllPlayerRankingPoints();
        }

        [DataTestMethod]
        [DataRow(8, 12)]
        [DataRow(8, -12)]
        [DataRow(0, 239)]
        [DataRow(7, 37)]
        [DataRow(10, -37)]
        [DataRow(6, 38)]
        [DataRow(13, -38)]
        [DataRow(5, 87)]
        [DataRow(16, -87)]
        [DataRow(4, 112)]
        [DataRow(20, -112)]
        [DataRow(3, 137)]
        [DataRow(25, -125)]
        [DataRow(2, 187)]
        [DataRow(30, -162)]
        [DataRow(35, -187)]
        [DataRow(1, 237)]
        [DataRow(45, -237)]
        [DataRow(50, -238)]
        [DataRow(50, -300)]
        public void ComputePointsAwaredTest(int expected, int value)
        {
            var points = RankingService.ComputePointsAwarded(value);
            Assert.AreEqual(expected, points);
        }

        [DataTestMethod]
        [DataRow(1, 2, -70)]
        [DataRow(1, 3, -131)]
        [DataRow(2,3, -61)]
        [DataRow(3,1, 131)]
        public void ComputePointSpreadTest(int player1Id, int player2Id, int expected)
        {
            var pointSpread = _rankingService.ComputePointSpread(player1Id, player2Id);
            Assert.AreEqual(expected, pointSpread);
        }

        [DataTestMethod]
        [DataRow(1,2,2,0)]
        [DataRow(2,3,3,6)]
        [DataRow(3,1,1,25)]
        public void ComputeMatchPointValuesForPlayerTest(int player1Id, int player2Id, int winningid, int expected)
        {
            var match = new Match
            {
                Player1Id = player1Id,
                Player2Id = player2Id,
                WinningPlayerId = winningid
            };
            var matchPoints = _rankingService.ComputeMatchPointValuesForPlayer(match, 1);
            Assert.AreEqual(expected, matchPoints);
        }
    }
}