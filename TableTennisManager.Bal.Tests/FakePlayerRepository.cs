﻿using System.Collections.Generic;
using TableTennisManager.Contracts;
using TableTennisManager.Models;

namespace TableTennisManager.Bal.Tests
{
    class FakePlayerRepository : IPlayerRepository
    {
        public Player GetById(int playerId)
        {
            if (playerId == 2)
            {
                return new Player
                {
                    FirstName = "Sally",
                    LastName = "Sue",
                    PlayerId = 2,
                    RankingPoints = 185, 
                    PlayerRating = 5.0m
                };
            }

            if (playerId == 3)
            {
                return new Player
                {
                    FirstName = "Sam",
                    LastName = "Smith",
                    PlayerId = 3,
                    RankingPoints = 246,
                    PlayerRating = 6.5m
                };
            }

            return new Player
            {
                FirstName = "John",
                LastName = "Doe",
                NickName = "Johnny",
                RankingPoints = 115,
                PlayerRating = 4.5m
            };
        }

        public IEnumerable<Player> GetByName(string name)
        {
            var playerList = new List<Player>
            {
                new Player
                {
                    FirstName = "John",
                    LastName = "Doe",
                    NickName = "Johnny",
                    PlayerId = 1
                },
                new Player
                {
                    FirstName = "Sally",
                    LastName = "Sue",
                    NickName = "Sal",
                    PlayerId = 2
                },
            };

            return playerList;
        }

        public IEnumerable<Player> GetPlayers()
        {
            var playerList = new List<Player>
            {
                new Player
                {
                    FirstName = "John",
                    LastName = "Doe",
                    NickName = "Johnny",
                    PlayerId = 1,
                    RankingPoints = 135
                },
                new Player
                {
                    FirstName = "Sally",
                    LastName = "Sue",
                    NickName = "Sal",
                    PlayerId = 2,
                    RankingPoints = 165
                },
                new Player
                {
                    FirstName = "Sam",
                    LastName = "Smith",
                    PlayerId = 3,
                    RankingPoints = 233
                }
            };

            return playerList;
        }

        public int Create(Player player)
        {
            return 1;
        }

        public void Update(Player player)
        {
        }
    }
}
