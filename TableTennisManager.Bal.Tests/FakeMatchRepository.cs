﻿using System;
using System.Collections.Generic;
using TableTennisManager.Contracts;
using TableTennisManager.Models;

namespace TableTennisManager.Bal.Tests
{
    class FakeMatchRepository : IMatchRepository
    {
        public Match GetById(int matchId)
        {
            throw new NotImplementedException();
        }

        private int IncrementPlayer2(int playerId, int lastIdUsed)
        {
            int newVal = lastIdUsed + 1;
            if (newVal > 3)
            {
                newVal = 1;
            }

            if (playerId == newVal)
            {
                newVal++;
            }

            return newVal;
        }
        public IEnumerable<Match> GetMatchesByPlayerId(int playerId)
        {

            var matches = new List<Match>
            {
                new Match
                {
                    Player1Id = playerId,
                    Player2Id = IncrementPlayer2(playerId, 0),
                    Player1Score = 21,
                    Player2Score = 17
                },
                new Match
                {
                    Player1Id = playerId,
                    Player2Id = IncrementPlayer2(playerId, 1),
                    Player1Score = 21,
                    Player2Score = 13
                },
                new Match
                {
                    Player1Id = playerId,
                    Player2Id = IncrementPlayer2(playerId, 2),
                    Player1Score = 21,
                    Player2Score = 8
                },
                new Match
                {
                    Player1Id = playerId,
                    Player2Id = IncrementPlayer2(playerId, 3),
                    Player1Score = 15,
                    Player2Score = 21
                },
            };

            return matches;
        }

        public IEnumerable<Match> GetMatches()
        {
            var matches = new List<Match>
            {
                new Match
                {
                    MatchDate = DateTime.Now,
                    Player1Id = 1,
                    Player2Id = 2,
                    WinningPlayerId = 1
                },
                new Match
                {
                    MatchDate = DateTime.Now,
                    Player1Id = 1,
                    Player2Id = 3,
                    WinningPlayerId = 1
                },
                new Match
                {
                    MatchDate = DateTime.Now.AddDays(-364),
                    Player1Id = 2,
                    Player2Id = 3,
                    WinningPlayerId = 3
                }
            };

            return matches;
        }

        public void Create(Match match)
        {
            throw new NotImplementedException();
        }
    }
}
