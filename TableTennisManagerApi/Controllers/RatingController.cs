﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using TableTennisManager.Bal.Services;
using TableTennisManager.Contracts;
using TableTennisManager.Dal;
using TableTennisManager.Models;

namespace TableTennisManagerApi.Controllers
{
    [Authorize]
    public class RatingController : ApiController
    {
        private TableTennisManagerContext _ttContext;
        private IMatchRepository _matchRepository;
        private IPlayerRepository _playerRepository;
        private IDependencyScope _container;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            _container = controllerContext.Configuration.DependencyResolver;
            _ttContext = (TableTennisManagerContext)_container.GetService(typeof(TableTennisManagerContext));
            _matchRepository = (IMatchRepository)_container.GetService(typeof(IMatchRepository));
            _playerRepository = (IPlayerRepository) _container.GetService(typeof(IPlayerRepository));
            base.Initialize(controllerContext);
        }

        [HttpGet, Route("GetPlayersByRating")]
        public async Task<IHttpActionResult> GetPlayersByRating(int topCount)
        {
            var playersByRating = GetPlayersOrderedByRating(topCount);
            return Ok(playersByRating);
        }

        [HttpGet, Route("CalculatePlayerRatings")]
        public async Task<IHttpActionResult> CalculatePlayerRatings()
        {
            var ratingService = new RatingService(_playerRepository, _matchRepository);
            ratingService.GenerateAllPlayerRatings();
            var playersByRatingUpdated = GetPlayersOrderedByRating(0);
            return Ok(playersByRatingUpdated);
        }

        private IEnumerable<Player> GetPlayersOrderedByRating(int topCount)
        {
            var players = _playerRepository.GetPlayers().OrderByDescending(p => p.PlayerRating);
            if (topCount > 0)
                return players.Take(topCount);

            return players;
        }
    }
}
