﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using TableTennisManager.Contracts;

namespace TableTennisManagerApi.Controllers
{
    public class LoginController : ApiController
    {
        private IPlayerRepository _playerRepository;
        private IDependencyScope _container;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            _container = controllerContext.Configuration.DependencyResolver;
            _playerRepository = (IPlayerRepository)_container.GetService(typeof(IPlayerRepository));
            base.Initialize(controllerContext);
        }

        [HttpPost, Route("ValidateLogin")]
        public async Task<IHttpActionResult> ValidateLogin(string userName, string passWord)
        {
            var player = _playerRepository.GetPlayers().FirstOrDefault(p => p.UserName == userName);

            if (player == null)
                return BadRequest("Invalid Username");

            var isValid = SecurePasswordHasher.Verify(passWord, player.PassWord);

            if (isValid)
                return Ok("Token Gets Returned");

            return BadRequest("Invalid Password");
        }
    }
}
