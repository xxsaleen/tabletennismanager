﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using TableTennisManager.Contracts;
using TableTennisManager.Dal;

namespace TableTennisManagerApi.Controllers
{
    [Authorize]
    public class RankingController : ApiController
    {
        private TableTennisManagerContext _ttContext;
        private IMatchRepository _matchRepository;
        private IPlayerRepository _playerRepository;
        private IDependencyScope _container;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            _container = controllerContext.Configuration.DependencyResolver;
            _ttContext = (TableTennisManagerContext)_container.GetService(typeof(TableTennisManagerContext));
            _matchRepository = (IMatchRepository)_container.GetService(typeof(IMatchRepository));
            _playerRepository = (IPlayerRepository)_container.GetService(typeof(IPlayerRepository));
            base.Initialize(controllerContext);
        }

        [HttpGet, Route("GetCurrentRankingList")]
        public async Task<IHttpActionResult> GetCurrentRankingList(int topCount)
        {
            var playersByRanking =
                _playerRepository.GetPlayers().OrderByDescending(p => p.RankingPoints).Take(topCount);
            return Ok(playersByRanking);
        }
    }
}
