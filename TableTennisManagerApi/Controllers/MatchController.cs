﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using TableTennisManager.Contracts;
using TableTennisManager.Dal;
using TableTennisManager.Models;

namespace TableTennisManagerApi.Controllers
{
    [Authorize]
    public class MatchController : ApiController
    {
        private TableTennisManagerContext _ttContext;
        private IMatchRepository _matchRepository;
        private IDependencyScope _container;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            _container = controllerContext.Configuration.DependencyResolver;
            _ttContext = (TableTennisManagerContext)_container.GetService(typeof(TableTennisManagerContext));
            _matchRepository = (IMatchRepository)_container.GetService(typeof(IMatchRepository));
            base.Initialize(controllerContext);
        }

        [HttpGet, Route("GetAllMatches")]
        public async Task<IHttpActionResult> GetAllMatches()
        {
            var matches = _matchRepository.GetMatches();
            return Ok(matches);
        }

        [HttpPost, Route("Create")]
        public async Task<IHttpActionResult> Create(Match match)
        {
            _matchRepository.Create(match);

            return Ok();
        }

        [HttpPost, Route("GetMatchesByPlayerId")]
        public async Task<IHttpActionResult> GetMatchesByPlayerId(int playerId)
        {
            var matches = _matchRepository.GetMatchesByPlayerId(playerId);
            return Ok(matches);
        }
    }
}
