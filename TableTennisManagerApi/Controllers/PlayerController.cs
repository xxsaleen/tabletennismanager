﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using TableTennisManager.Contracts;
using TableTennisManager.Dal;
using TableTennisManager.Models;

namespace TableTennisManagerApi.Controllers
{
    [Authorize]
    public class PlayerController : ApiController
    {
        private TableTennisManagerContext _ttContext;
        private IPlayerRepository _playerRepository;
        private IDependencyScope _container;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            _container = controllerContext.Configuration.DependencyResolver;
            _ttContext = (TableTennisManagerContext)_container.GetService(typeof(TableTennisManagerContext));
            _playerRepository = (IPlayerRepository)_container.GetService(typeof(IPlayerRepository));
            base.Initialize(controllerContext);
        }

        public async Task<IHttpActionResult> GetPlayers()
        {
            return Ok(_playerRepository.GetPlayers());
        }

        public async Task<IHttpActionResult> Create(Player player)
        {
            player.PassWord = SecurePasswordHasher.Hash(player.PassWord);
            return Ok(_playerRepository.Create(player));
        }
    }
}
