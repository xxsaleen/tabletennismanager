﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Swashbuckle.Application;
using System;
using System.Configuration;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TableTennisManager.Contracts;
using TableTennisManager.Dal;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

[assembly: OwinStartup(typeof(TableTennisManagerApi.Startup))]
namespace TableTennisManagerApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var container = new UnityContainer();
            var ttContext = new TableTennisManagerContext(ConfigurationManager.AppSettings["dbConnectionString"]);
            container.RegisterType<IPlayerRepository, PlayerRepository>(new ContainerControlledLifetimeManager(), new InjectionConstructor(ttContext));
            container.RegisterType<IMatchRepository, MatchRepository>(new ContainerControlledLifetimeManager(), new InjectionConstructor(ttContext));
            container.RegisterInstance(typeof(TableTennisManagerContext), ttContext);
            config.DependencyResolver = new UnityResolver(container);

            var oAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(60),
                Provider = new SimpleAuthorizationServerProvider(ttContext)
            };

            app.UseOAuthBearerTokens(oAuthOptions);
            app.UseOAuthAuthorizationServer(oAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            app.UseWebApi(config);
            ConfigureWebApi(config);
            ConfigureSwagger(config);
        }

        private void ConfigureWebApi(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            // Web API routes
            config.MapHttpAttributeRoutes();
        }

        private void ConfigureSwagger(HttpConfiguration config)
        {
            var containingAssembly = typeof(Startup).Assembly;
            config
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "Matching Api");
                    c.IgnoreObsoleteActions();
                    c.RootUrl(req => new Uri(req.RequestUri, req.GetRequestContext().VirtualPathRoot).ToString());
                })
                .EnableSwaggerUi(c =>
                {
                });
        }
    }
}