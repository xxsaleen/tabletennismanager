﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using TableTennisManager.Dal;

namespace TableTennisManagerApi
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {

        private readonly TableTennisManagerContext _ttContext;
        public SimpleAuthorizationServerProvider(TableTennisManagerContext context)
        {
            _ttContext = context;
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); //   
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            if (_ttContext != null)
            {
                var player = _ttContext.Players.FirstOrDefault(p => p.UserName == context.UserName);

                if (player != null)
                {
                    var isPassWordValid = SecurePasswordHasher.Verify(context.Password, player.PassWord);
                    if (isPassWordValid)
                    {
                        identity.AddClaim(new Claim("Age", "16"));

                        var props = new AuthenticationProperties(new Dictionary<string, string>
                            {
                                {
                                    "userdisplayname", context.UserName
                                },
                                {
                                     "role", "admin"
                                }
                             });

                        var ticket = new AuthenticationTicket(identity, props);
                        context.Validated(ticket);
                    }
                    else
                    {
                        context.SetError("invalid_grant", "Provided username and password is incorrect");
                        context.Rejected();
                    }
                }
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                context.Rejected();
            }
            return;
        }
    }
}