﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TableTennisManager.Bal.Services;
using TableTennisManager.Contracts;
using TableTennisManager.Dal;

namespace TableTennisManager.Bal.IntegrationTests.Services
{
    [TestClass]
    public class RatingServiceTests
    {
        private TableTennisManagerContext _ttContext;
        private IMatchRepository _matchRepository;
        private IPlayerRepository _playerRepository;

        [TestInitialize]
        public void Init()
        {
            _ttContext = new TableTennisManagerContext(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TableTennisManager.Dal.TableTennisManagerContext;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            _matchRepository = new MatchRepository(_ttContext);
            _playerRepository= new PlayerRepository(_ttContext);
        }

        [TestMethod]
        public void GenerateAllPlayerRatingsTest()
        {
            Assert.Fail();
        }

        [TestMethod]
        public void CalculatePlayerRatingTest()
        {
            var ratingService = new RatingService(_playerRepository, _matchRepository);
            var rating = ratingService.CalculatePlayerRating(4);

            Console.WriteLine(rating);
        }
    }
}