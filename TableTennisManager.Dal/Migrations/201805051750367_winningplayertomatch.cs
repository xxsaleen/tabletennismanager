namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class winningplayertomatch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matches", "WinningPlayerId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Matches", "WinningPlayerId");
        }
    }
}
