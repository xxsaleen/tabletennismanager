namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredfields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Players", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Players", "LastName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Players", "LastName", c => c.String());
            AlterColumn("dbo.Players", "FirstName", c => c.String());
        }
    }
}
