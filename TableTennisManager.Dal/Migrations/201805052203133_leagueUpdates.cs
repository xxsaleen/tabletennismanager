namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class leagueUpdates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matches", "LeagueId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Matches", "LeagueId");
        }
    }
}
