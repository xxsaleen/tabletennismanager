namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class playerrating : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Players", "PlayerRating", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Players", "PlayerRating");
        }
    }
}
