namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixmatchtable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Matches", "HomePlayer_PlayerId", "dbo.Players");
            DropForeignKey("dbo.Matches", "Player1_PlayerId", "dbo.Players");
            DropForeignKey("dbo.Matches", "Player2_PlayerId", "dbo.Players");
            DropIndex("dbo.Matches", new[] { "HomePlayer_PlayerId" });
            DropIndex("dbo.Matches", new[] { "Player1_PlayerId" });
            DropIndex("dbo.Matches", new[] { "Player2_PlayerId" });
            AddColumn("dbo.Matches", "HomePlayerId", c => c.Int(nullable: false));
            DropColumn("dbo.Matches", "HomePlayer_PlayerId");
            DropColumn("dbo.Matches", "Player1_PlayerId");
            DropColumn("dbo.Matches", "Player2_PlayerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Matches", "Player2_PlayerId", c => c.Int());
            AddColumn("dbo.Matches", "Player1_PlayerId", c => c.Int());
            AddColumn("dbo.Matches", "HomePlayer_PlayerId", c => c.Int());
            DropColumn("dbo.Matches", "HomePlayerId");
            CreateIndex("dbo.Matches", "Player2_PlayerId");
            CreateIndex("dbo.Matches", "Player1_PlayerId");
            CreateIndex("dbo.Matches", "HomePlayer_PlayerId");
            AddForeignKey("dbo.Matches", "Player2_PlayerId", "dbo.Players", "PlayerId");
            AddForeignKey("dbo.Matches", "Player1_PlayerId", "dbo.Players", "PlayerId");
            AddForeignKey("dbo.Matches", "HomePlayer_PlayerId", "dbo.Players", "PlayerId");
        }
    }
}
