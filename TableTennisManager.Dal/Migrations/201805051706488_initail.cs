namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Matches",
                c => new
                    {
                        MatchId = c.Int(nullable: false, identity: true),
                        Player1Id = c.Int(nullable: false),
                        Player2Id = c.Int(nullable: false),
                        MatchDate = c.DateTime(nullable: false),
                        Player1Score = c.Int(nullable: false),
                        Player2Score = c.Int(nullable: false),
                        HomePlayer_PlayerId = c.Int(),
                        Player1_PlayerId = c.Int(),
                        Player2_PlayerId = c.Int(),
                    })
                .PrimaryKey(t => t.MatchId)
                .ForeignKey("dbo.Players", t => t.HomePlayer_PlayerId)
                .ForeignKey("dbo.Players", t => t.Player1_PlayerId)
                .ForeignKey("dbo.Players", t => t.Player2_PlayerId)
                .Index(t => t.HomePlayer_PlayerId)
                .Index(t => t.Player1_PlayerId)
                .Index(t => t.Player2_PlayerId);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        PlayerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        NickName = c.String(),
                    })
                .PrimaryKey(t => t.PlayerId);
            
            CreateTable(
                "dbo.Rankings",
                c => new
                    {
                        RankingId = c.Int(nullable: false, identity: true),
                        PlayerId = c.Int(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        CurrentRanking = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RankingId)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rankings", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Matches", "Player2_PlayerId", "dbo.Players");
            DropForeignKey("dbo.Matches", "Player1_PlayerId", "dbo.Players");
            DropForeignKey("dbo.Matches", "HomePlayer_PlayerId", "dbo.Players");
            DropIndex("dbo.Rankings", new[] { "PlayerId" });
            DropIndex("dbo.Matches", new[] { "Player2_PlayerId" });
            DropIndex("dbo.Matches", new[] { "Player1_PlayerId" });
            DropIndex("dbo.Matches", new[] { "HomePlayer_PlayerId" });
            DropTable("dbo.Rankings");
            DropTable("dbo.Players");
            DropTable("dbo.Matches");
        }
    }
}
