namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rankingpointsforplayer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Players", "RankingPoints", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Players", "RankingPoints");
        }
    }
}
