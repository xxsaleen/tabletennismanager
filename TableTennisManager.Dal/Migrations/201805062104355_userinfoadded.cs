namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userinfoadded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Players", "Email", c => c.String(nullable: false));
            AddColumn("dbo.Players", "UserName", c => c.String(nullable: false));
            AddColumn("dbo.Players", "PassWord", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Players", "PassWord");
            DropColumn("dbo.Players", "UserName");
            DropColumn("dbo.Players", "Email");
        }
    }
}
