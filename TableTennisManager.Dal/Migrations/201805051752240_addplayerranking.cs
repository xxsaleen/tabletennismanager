namespace TableTennisManager.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addplayerranking : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Players", "CurrentRanking", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Players", "CurrentRanking");
        }
    }
}
