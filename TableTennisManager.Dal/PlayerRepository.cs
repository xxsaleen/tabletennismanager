﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TableTennisManager.Contracts;
using TableTennisManager.Models;

namespace TableTennisManager.Dal
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly TableTennisManagerContext _ttContext;

        public PlayerRepository(TableTennisManagerContext ttContext)
        {
            _ttContext = ttContext;
        }

        public Player GetById(int playerId)
        {
            return _ttContext.Players.FirstOrDefault(p => p.PlayerId == playerId);
        }

        public IEnumerable<Player> GetByName(string name)
        {
            return _ttContext.Players.Where(p => p.FirstName.Contains(name) || p.LastName.Contains(name));
        }

        public IEnumerable<Player> GetPlayers()
        {
            return _ttContext.Players.ToList();
        }

        public int Create(Player player)
        {
            _ttContext.Players.Add(player);
            return _ttContext.SaveChanges();
        }

        public void Update(Player player)
        {
            _ttContext.Players.Attach(player);
            _ttContext.Entry(player).State = EntityState.Modified;
            _ttContext.SaveChanges();
        }
    }
}
