﻿using System.Collections.Generic;
using System.Linq;
using TableTennisManager.Contracts;
using TableTennisManager.Models;

namespace TableTennisManager.Dal
{
    public class MatchRepository : IMatchRepository
    {
        private readonly TableTennisManagerContext _ttContext;
        public MatchRepository(TableTennisManagerContext ttContext)
        {
            _ttContext = ttContext;
        }
        public Match GetById(int matchId)
        {
            return _ttContext.Matches.FirstOrDefault(m => m.MatchId == matchId);
        }

        public IEnumerable<Match> GetMatchesByPlayerId(int playerId)
        {
            return _ttContext.Matches.Where(m => m.Player1Id == playerId || m.Player2Id == playerId);
        }

        public IEnumerable<Match> GetMatches()
        {
            return _ttContext.Matches.ToList();
        }

        public void Create(Match match)
        {
            _ttContext.Matches.Add(match);
            _ttContext.SaveChanges();
        }
    }
}
