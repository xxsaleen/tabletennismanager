﻿using System.Data.Entity;
using TableTennisManager.Models;

namespace TableTennisManager.Dal
{
    public class TableTennisManagerContext : DbContext
    {
        public TableTennisManagerContext() { }
        public TableTennisManagerContext(string connString) : base(connString)
        {}
        public DbSet<Match> Matches { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Ranking> Rankings { get; set; }
    }
}
