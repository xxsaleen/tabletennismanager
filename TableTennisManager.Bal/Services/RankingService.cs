﻿using System;
using System.Collections.Generic;
using System.Linq;
using TableTennisManager.Contracts;
using TableTennisManager.Models;

namespace TableTennisManager.Bal.Services
{
    public class RankingService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IPlayerRepository _playerRepository;

        public RankingService(IMatchRepository matchRepository, IPlayerRepository playerRepository)
        {
            _matchRepository = matchRepository;
            _playerRepository = playerRepository;
        }

        public IEnumerable<Player> CalculatePlayerRankings()
        {
            var playerRankings = _playerRepository.GetPlayers().OrderByDescending(p => p.RankingPoints).ToList();

            var ranking = 1;
            foreach (var player in playerRankings)
            {
                player.CurrentRanking = ranking;
                _playerRepository.Update(player);
                ranking += 1;
            }

            return playerRankings;
        }

        public IEnumerable<Player> CalculateAllPlayerRankingPoints()
        {
            var players = _playerRepository.GetPlayers().ToList();
            foreach (var player in players)
            {
                player.RankingPoints = CalculatePlayerRankingPoints(player);
                _playerRepository.Update(player);
            }

            return players;
        }

        public int CalculatePlayerRankingPoints(Player player)
        {
            var matchPointsEarned = new List<int>();

            var playerMatches = _matchRepository.GetMatches()
                .Where(m => (m.Player1Id == player.PlayerId || m.Player2Id == player.PlayerId) && m.MatchDate >= DateTime.Now.AddDays(-365)).ToList();

            var baselinePoints = 100;
            foreach (var match in playerMatches)
            {
                var matchPoints = ComputeMatchPointValuesForPlayer(match, player.PlayerId);
                matchPointsEarned.Add(matchPoints);
            }

            var pointsFromMatches = matchPointsEarned.OrderByDescending(p => p).Take(10).Sum();
            var totalPoints = pointsFromMatches + baselinePoints;
            return totalPoints;
        }

        public int ComputeMatchPointValuesForPlayer(Match match, int playerId)
        {
            //you only get points by winning matches
            if (match.WinningPlayerId != playerId)
                return 0;

            var player2Id = match.Player1Id == playerId ? match.Player2Id : match.Player1Id;
            var pointSpread = ComputePointSpread(playerId, player2Id);
            return ComputePointsAwarded(pointSpread);
        }

        public static int ComputePointsAwarded(int pointSpread)
        {
            if (pointSpread <= -238)
            {
                return 50;
            }
            if (pointSpread >= -237 && pointSpread <= -213)
            {
                return 45;
            }
            if (pointSpread >= -212 && pointSpread <= -188)
            {
                return 40;
            }
            if (pointSpread >= -187 && pointSpread <= -163)
            {
                return 35;
            }
            if (pointSpread >= -162 && pointSpread <= -138)
            {
                return 30;
            }
            if (pointSpread >= -137 && pointSpread <= -113)
            {
                return 25;
            }
            if (pointSpread >= -112 && pointSpread <= -88)
            {
                return 20;
            }
            if (pointSpread >= -87 && pointSpread <= -63)
            {
                return 16;
            }
            if (pointSpread >= -62 && pointSpread <= -38)
            {
                return 13;
            }
            if (pointSpread >= -37 && pointSpread <= -13)
            {
                return 10;
            }
            if ((pointSpread >= -12 && pointSpread <= 0) || (pointSpread >= 0 && pointSpread <= 12))
            {
                return 8;
            }

            if (pointSpread >= 13 & pointSpread <= 37)
            {
                return 7;
            }

            if (pointSpread >= 38 && pointSpread <= 62)
            {
                return 6;
            }

            if (pointSpread >= 63 && pointSpread <= 87)
            {
                return 5;
            }

            if (pointSpread >= 88 && pointSpread <= 112)
            {
                return 4;
            }

            if (pointSpread >= 113 && pointSpread <= 137)
            {
                return 3;
            }

            if (pointSpread >= 138 && pointSpread <= 187)
            {
                return 2;
            }

            if (pointSpread >= 188 && pointSpread <= 237)
            {
                return 1;
            }

            return 0;
        }

        //returns the point spread based on how player1 compares to player2
        public int ComputePointSpread(int player1Id, int player2Id)
        {
            var player1 = _playerRepository.GetById(player1Id);
            var player2 = _playerRepository.GetById(player2Id);

            return player1.RankingPoints - player2.RankingPoints;
        }

    }
}
