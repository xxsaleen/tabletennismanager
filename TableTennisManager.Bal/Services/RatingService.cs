﻿using System.Collections.Generic;
using System.Linq;
using TableTennisManager.Contracts;
using TableTennisManager.Models;

namespace TableTennisManager.Bal.Services
{
    public class RatingService
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IMatchRepository _matchRepository;

        public RatingService(IPlayerRepository playerRepository, IMatchRepository matchRepository)
        {
            _playerRepository = playerRepository;
            _matchRepository = matchRepository;
        }

        public IEnumerable<Player> GenerateAllPlayerRatings()
        {
            var players = _playerRepository.GetPlayers().ToList();

            foreach (var player in players)
            {
                player.PlayerRating = CalculatePlayerRating(player.PlayerId);
                _playerRepository.Update(player);
            }

            return players;
        }

        public decimal CalculatePlayerRating(int playerId)
        {
            var matches = _matchRepository.GetMatchesByPlayerId(playerId).ToList();
                
            if (matches.Count == 0)
                return _playerRepository.GetById(playerId).PlayerRating;

            var totalOpponentCount = 0;
            var totalOpponentRating = 0m;
            var totalOpponentPoints = 0;
            var totalPlayerPoints = 0;

            foreach (var match in matches)
            {
                totalOpponentCount += 1;
                int opponentId;
                if (match.Player1Id == playerId)
                {
                    opponentId = match.Player2Id;
                    totalPlayerPoints += match.Player1Score;
                    totalOpponentPoints += match.Player2Score;
                }
                else
                {
                    opponentId = match.Player1Id;
                    totalPlayerPoints += match.Player2Score;
                    totalOpponentPoints += match.Player1Score;
                }

                var opponent = _playerRepository.GetById(opponentId);
                totalOpponentRating += opponent.PlayerRating;
            }

            var averageOpponentRating = decimal.Divide(totalOpponentRating, totalOpponentCount);
            var averageOpponentScore = decimal.Divide(totalOpponentPoints, totalOpponentCount);
            var averagePlayerScore = decimal.Divide(totalPlayerPoints, totalOpponentCount);
            var ratingMultiplier = decimal.Divide(averagePlayerScore, averageOpponentScore);
            var playerRating = averageOpponentRating * ratingMultiplier;
            return ValidateRating(playerRating);
        }

        private decimal ValidateRating(decimal rating)
        {
            if (rating < 1m)
                return 1m;

            if (rating > 7m)
                return 7m;

            return rating;
        }

    }
}
