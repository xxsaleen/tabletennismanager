﻿using System.Collections.Generic;
using TableTennisManager.Models;

namespace TableTennisManager.Contracts
{
    public interface IPlayerRepository
    {
        Player GetById(int playerId);
        IEnumerable<Player> GetByName(string name);
        IEnumerable<Player> GetPlayers();
        int Create(Player player);
        void Update(Player player);
    }
}
