﻿using System.Collections.Generic;
using TableTennisManager.Models;

namespace TableTennisManager.Contracts
{
    public interface IMatchRepository
    {
        Match GetById(int matchId);
        IEnumerable<Match> GetMatchesByPlayerId(int playerId);
        IEnumerable<Match> GetMatches();
        void Create(Match match);
    }
}
