﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TableTennisManager.Models
{
    public class Match
    {
        [Required]
        public int MatchId { get; set; }
        public int LeagueId { get; set; }
        [Required]
        public int Player1Id { get; set; }
        [Required]
        public int Player2Id { get; set; }
        [Required]
        public DateTime MatchDate { get; set; }
        public int HomePlayerId { get; set; }
        [Required]
        public int Player1Score { get; set; }
        [Required]
        public int Player2Score { get; set; }
        [Required]
        public int WinningPlayerId { get; set; }
    }
}
