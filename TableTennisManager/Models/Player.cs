﻿using System.ComponentModel.DataAnnotations;

namespace TableTennisManager.Models
{
    public class Player
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string NickName { get; set; }
        [Required]
        public int PlayerId { get; set; }
        public int RankingPoints { get; set; }
        public int CurrentRanking { get; set; }
        public decimal PlayerRating { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string PassWord { get; set; }

        public Player()
        {
            RankingPoints = 100;
        }
    }
}
