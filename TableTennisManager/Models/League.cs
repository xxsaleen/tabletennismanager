﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TableTennisManager.Models
{
    public class League
    {
        [Required]
        public int LeagueId { get; set; }
        [Required]
        public string Name { get; set; }
        public List<int> PlayerIds { get; set; }
        public List<int> MatchIds { get; set; }
    }
}
