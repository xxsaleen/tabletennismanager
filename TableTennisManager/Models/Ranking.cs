﻿using System;

namespace TableTennisManager.Models
{
    public class Ranking
    {
        public int RankingId { get; set; }
        public Player Player { get; set; }
        public int PlayerId { get; set; }
        public DateTime DateTime { get; set; }
        public int CurrentRanking { get; set; }
    }
}
